<?php

declare(strict_types=1);

namespace asmaru\gdlib;

use function getimagesize;

/**
 * Class ImageInfo
 *
 * @package asmaru\gdlib
 */
class ImageInfo {

	private int $width = 0;

	private int $height = 0;

	/**
	 * ImageInfo constructor.
	 */
	public function __construct(Image $image) {
		[$width, $height] = getimagesize($image->getRealPath());
		if ($width > 0 && $height > 0) {
			$this->width = $width;
			$this->height = $height;
		}
	}

	public function getWidth(): int {
		return $this->width;
	}

	public function getHeight(): int {
		return $this->height;
	}
}