<?php

declare(strict_types=1);

namespace asmaru\gdlib;

class ImageDimension {

	public function __construct(private readonly int $width, private readonly int $height) {
	}

	public function getHeight(): int {
		return $this->height;
	}

	public function getWidth(): int {
		return $this->width;
	}
}