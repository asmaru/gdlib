<?php

declare(strict_types=1);

namespace asmaru\gdlib;

use GdImage;
use InvalidArgumentException;
use function getimagesize;
use function image_type_to_extension;
use function imagecopyresampled;
use function imagecreatefromgif;
use function imagecreatefromjpeg;
use function imagecreatefrompng;
use function imagecreatefromwebp;
use function imagecreatetruecolor;
use function imagedestroy;
use function imagejpeg;
use function imagepng;
use function imagesx;
use function imagesy;
use function imagewebp;
use function is_array;
use function is_file;
use function min;
use function sprintf;
use const IMAGETYPE_GIF;
use const IMAGETYPE_JPEG;
use const IMAGETYPE_PNG;
use const IMAGETYPE_WEBP;

class GDUtility {

	/**
	 * Checks if the given file exists and is a valid image.
	 */
	public static function isImage(string $file): bool {
		return is_file($file) && is_array(@getimagesize($file));
	}

	/**
	 * Get the dimensions for the given image.
	 */
	public static function getImageInfo(string $file): ?ImageInfo {
		try {
			return (new Image($file))->getImageInfo();
		} catch (InvalidArgumentException) {
		}
		return null;
	}

	/**
	 * Takes a image file and creates a resized copy. The extension will be added to the destination path.
	 *
	 * Supported image formats: jpg, png, gif
	 */
	public static function toJPG(string $file, string $destination, int $width = null, int $height = null, int $quality = 75): Image {
		return self::process($file, $destination, $width, $height, $quality, IMAGETYPE_JPEG);
	}

	/**
	 * Takes a image file and creates a resized copy. The extension will be added to the destination path.
	 */
	public static function toPNG(string $file, string $destination, int $width = null, int $height = null, int $quality = -1): Image {
		return self::process($file, $destination, $width, $height, $quality, IMAGETYPE_PNG);
	}

	/**
	 * Takes a image file and creates a resized copy. The extension will be added to the destination path.
	 */
	public static function toWEBP(string $file, string $destination, int $width = null, int $height = null, int $quality = -1): Image {
		return self::process($file, $destination, $width, $height, $quality, IMAGETYPE_WEBP);
	}

	public static function getFilenameWithExtension(string $name, $type = IMAGETYPE_JPEG): string {
		return $name . image_type_to_extension($type);
	}

	public static function load(string $file): ?GdImage {
		$image = null;
		if (is_file($file)) {
			/** @noinspection PhpUnusedLocalVariableInspection */
			[$width, $height, $type] = @getimagesize($file);
			switch ($type) {
				case IMAGETYPE_GIF:
					$image = imagecreatefromgif($file);
					break;
				case IMAGETYPE_JPEG:
					$image = imagecreatefromjpeg($file);
					break;
				case IMAGETYPE_PNG:
					$image = imagecreatefrompng($file);
					break;
				case IMAGETYPE_WEBP:
					$image = imagecreatefromwebp($file);
					break;
			}
		}
		return $image;
	}

	public static function getDimensions(string $file): ImageDimension {
		$source = self::load($file);
		if ($source !== null) {
			return new ImageDimension(imagesx($source), imagesy($source));
		}
		return new ImageDimension(0, 0);
	}

	private static function process(string $file, string $destination, int $width, ?int $height, int $quality, int $type): Image {
		$source = self::load($file);
		if ($source !== null) {
			$sourceWidth = imagesx($source);
			$sourceHeight = imagesy($source);
			// do not upscale, use the given width or the original with if this value is smaller
			$width = min($sourceWidth, $width);
			$height ??= $sourceHeight / ($sourceWidth / $width);
			$newImage = imagecreatetruecolor($width, (int)$height);
			if (imagecopyresampled($newImage, $source, 0, 0, 0, 0, $width, (int)$height, $sourceWidth, $sourceHeight)) {
				$image = match ($type) {
					IMAGETYPE_PNG => imagepng($newImage, $destination, $quality),
					IMAGETYPE_WEBP => imagewebp($newImage, $destination, $quality),
					default => imagejpeg($newImage, $destination, $quality),
				};
				if ($image) {
					if (imagedestroy($newImage) && imagedestroy($source)) {
						return new Image($destination);
					}
				}
			}
		}
		throw new InvalidArgumentException(sprintf('Can not open file "%s" as image', $file));
	}
}