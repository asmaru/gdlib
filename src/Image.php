<?php

declare(strict_types=1);

namespace asmaru\gdlib;

use InvalidArgumentException;
use SplFileInfo;
use function filesize;
use function getimagesize;
use function is_array;
use function is_file;
use function sprintf;

/**
 * Class Image
 *
 * @package asmaru\gdlib
 */
class Image extends SplFileInfo {

	/**
	 * Image constructor.
	 *
	 * @param $file_name
	 */
	public function __construct($file_name) {
		if (!is_file($file_name) || filesize($file_name) === 0 || !is_array(@getimagesize($file_name))) throw new InvalidArgumentException(sprintf('File "%s" is not an image', $file_name));
		parent::__construct($file_name);
	}

	public function getImageInfo(): ImageInfo {
		return new ImageInfo($this);
	}

	/**
	 * @return array
	 */
	public function getDimensions(): ImageDimension {
		return GDUtility::getDimensions($this->getRealPath());
	}
}