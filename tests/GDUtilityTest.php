<?php

namespace asmaru\cms\util;

use asmaru\gdlib\GDUtility;
use FilesystemIterator;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use function getimagesize;
use const IMAGETYPE_JPEG;
use const IMAGETYPE_PNG;
use const IMAGETYPE_WEBP;

/**
 * Class GDUtilityTest
 *
 * @package asmaru\cms\util
 * @covers \asmaru\gdlib\GDUtility
 */
class GDUtilityTest extends TestCase {

	private string $testDir;

	public function testCanResize() {
		$samples = ['resize_sample.jpg', 'resize_sample.png', 'resize_sample.gif', 'resize_sample.webp'];
		foreach ($samples as $sample) {
			$path = $this->getResourceFile($sample);
			$newImage = $this->getTempFile(md5(uniqid(true)));
			$newImage = GDUtility::toJPG($path, $newImage, 200);
			$this->assertNotEmpty($newImage);
			[$width, $height, $type] = getimagesize($newImage);
			$this->assertEquals(200, $width);
			$this->assertEquals(133, $height);
			$this->assertEquals(IMAGETYPE_JPEG, $type);

			$newImage = GDUtility::toPNG($path, $newImage, 200);
			$this->assertNotEmpty($newImage);
			[$width, $height, $type] = getimagesize($newImage);
			$this->assertEquals(200, $width);
			$this->assertEquals(133, $height);
			$this->assertEquals(IMAGETYPE_PNG, $type);

			$newImage = GDUtility::toWEBP($path, $newImage, 200);
			$this->assertNotEmpty($newImage);
			[$width, $height, $type] = getimagesize($newImage);
			$this->assertEquals(200, $width);
			$this->assertEquals(133, $height);
			$this->assertEquals(IMAGETYPE_WEBP, $type);
		}
	}

	public function testCanResizeHeight() {
		$samples = ['resize_sample.jpg', 'resize_sample.png', 'resize_sample.gif', 'resize_sample.webp'];
		foreach ($samples as $sample) {
			$path = $this->getResourceFile($sample);
			$newImage = $this->getTempFile(md5(uniqid(true)));
			$newImage = GDUtility::toJPG($path, $newImage, 200, 200);
			$this->assertNotEmpty($newImage);
			[$width, $height, $type] = getimagesize($newImage);
			$this->assertEquals(200, $width);
			$this->assertEquals(200, $height);
			$this->assertEquals(IMAGETYPE_JPEG, $type);
		}
	}

	public function testCheckInvalidImages() {
		$path = $this->getResourceFile('no_image.txt');
		$this->expectException(InvalidArgumentException::class);
		GDUtility::toJPG($path, '', 200, 200);
	}

	public function testIsImage() {
		$this->assertTrue(GDUtility::isImage($this->getResourceFile('resize_sample.jpg')));
		$this->assertFalse(GDUtility::isImage($this->getResourceFile('no_image.txt')));
	}

	public function testGetDimensions() {
		$dimensions = GDUtility::getImageInfo($this->getResourceFile('resize_sample.jpg'));
		$this->assertNotNull($dimensions);
		$this->assertEquals(640, $dimensions->getWidth());
		$this->assertEquals(426, $dimensions->getHeight());

		$dimensions = GDUtility::getImageInfo($this->getResourceFile('no_image.txt'));
		$this->assertNull($dimensions);
	}

	public function testCanGetFilenameWithExtension() {
		$this->assertEquals('image.jpeg', GDUtility::getFilenameWithExtension('image'));
		$this->assertEquals('image.jpeg', GDUtility::getFilenameWithExtension('image', IMAGETYPE_JPEG));
		$this->assertEquals('image.png', GDUtility::getFilenameWithExtension('image', IMAGETYPE_PNG));
		$this->assertEquals('image.webp', GDUtility::getFilenameWithExtension('image', IMAGETYPE_WEBP));
	}

	private function getResourceFile($file): string {
		return __DIR__ . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . $file;
	}

	private function getTempFile($file): string {
		return $this->testDir . DIRECTORY_SEPARATOR . $file;
	}

	public function setUp(): void {
		$this->testDir = __DIR__ . DIRECTORY_SEPARATOR . md5(uniqid(true));
		mkdir($this->testDir);
	}

	public function tearDown(): void {
		$path = $this->testDir;
		if (is_file($path)) {
			unlink($path);
		} else {
			$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($iterator as $filename => $fileInfo) {
				if ($fileInfo->isDir()) {
					rmdir($filename);
				} else {
					unlink($filename);
				}
			}
			rmdir($path);
		}
	}
}